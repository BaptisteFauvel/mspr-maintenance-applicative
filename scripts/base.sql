create table districts
(
	id varchar not null
		constraint districts_pk
			primary key
);

alter table districts owner to admin;

create unique index districts_id_uindex
	on districts (id);

create table hours
(
	day date not null,
	district varchar not null
		constraint hours_pk
			primary key
		constraint hours_districts_id_fk
			references districts
				on update cascade on delete cascade,
	"end" varchar not null,
	start varchar not null
);

alter table hours owner to admin;

create table lamps
(
	id varchar not null
		constraint lamps_pk
			primary key,
	district varchar not null
		constraint lamps_districts_id_fk
			references districts
				on update cascade on delete cascade,
	latitude varchar not null,
	longitude varchar not null
);

alter table lamps owner to admin;

create unique index lamps_id_uindex
	on lamps (id);

create table spaces
(
	id varchar not null
		constraint public_spaces_pk
			primary key,
	name varchar not null,
	address varchar not null
);

alter table spaces owner to admin;

create unique index public_spaces_address_uindex
	on spaces (address);

create table tasks
(
	name varchar not null,
	state boolean not null,
	end_date date not null,
	space varchar not null
		constraint tasks_spaces_id_fk
			references spaces
				on update cascade on delete cascade,
	constraint tasks_pk
		primary key (name, end_date, space)
);

alter table tasks owner to admin;

