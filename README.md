# mspr maintenance applicative

Projet MSPR qui permet d'exposer les données de la ville de Paris via des API CSV, afin de les rendre compatibles avec les standards modernes d’OpenData. Pour cela vous devrez échanger avec les anciens systèmes dans leur format XML natif et convertir leurs réponses en CSV

## Installation

Pour installer toutes les dépendances du projet

```bash
npm install
```

pour installer la base de données et sonarqube en local

```bash
docker-compose up -d
```

une fois les deux servide lancé exécuté ces commandes dans un terminal pour alimenter la base de données :

```bash
docker exec -it mspr-maintenance-app
psql -h maintenance-app -d projet -U admin -f infile
```

pour ajouter le projet à sonarqube, connecter vous à l'interface sonarqube

user : admin
password : admin

puis créer une crée en ajoutant un projet puis copier cette clée dans le fichier  sonar-project.js à laracine du projet

## Utilisation

Pour démarrer le serveur

```bash
npm start
```

pour lancer une analyse sonarqube

```bash
npm run sonar-scanner
```
