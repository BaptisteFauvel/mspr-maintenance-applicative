const moment = require('moment');
const pool = require('../db/config');

module.exports = {
    async getWip() {
        return pool.query('select * from spaces where id in (select space from tasks where state = false)').then((result) => {
            const returnValues = result.rows.map((row) => pool.query('select end_date from tasks where space = $1 and state = false;', [row.id])
                .then((endDates) => {
                    const sortedEndDates = endDates.rows.sort((a, b) => b.end_date - a.end_date);
                    return ({
                        name: row.name,
                        address: row.address,
                        endDate: sortedEndDates[0].end_date
                    });
                }));

            return Promise.all(returnValues).then((data) => data);
        });
    },

    async getOpen() {
        return pool.query('select * from spaces where id not in (select space from tasks where state = false)').then((result) => {
            const returnValues = result.rows.map((row) => ({ name: row.name, address: row.address }));
            return returnValues;
        });
    }
};
