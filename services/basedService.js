const pool = require('../db/config');

module.exports = {
    async getHours(date) {
        return pool.query('SELECT * FROM hours WHERE day = $1', [date]).then((result) => {
            if (result.rows.length > 0) {
                const returnData = result.rows.map((row) => ({
                    district: row.district,
                    end: row.end,
                    start: row.start
                }));
                return returnData;
            }

            throw new Error('no data found');
        });
    }
};
