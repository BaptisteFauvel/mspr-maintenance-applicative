const express = require('express');
const sedatifService = require('../services/sedatifService');

const router = express.Router();

/**
 * @api {get} /sedatif/wip Retourne les espaces en travaux
 * @apiName Wip
 * @apiGroup Sedatif
 *
 * @apiSuccess {String} name Nom de l'espace
 * @apiSuccess {Time} address  adresse de l'espace
 * @apiSuccess {Time} endDate  date de fin des travaux
 */
router.get('/wip', (req, res, next) => {
    sedatifService.getWip().then((data) => {
        res.send(data);
    });
});

/**
 * @api {get} /sedatif/open Retourne les espaces ouverts
 * @apiName open
 * @apiGroup Sedatif
 *
 * @apiSuccess {String} name Nom de l'espace
 * @apiSuccess {Time} address  adresse de l'espace
 */
router.get('/open', (req, res, next) => {
    sedatifService.getOpen().then((data) => {
        res.send(data);
    });
});
module.exports = router;
