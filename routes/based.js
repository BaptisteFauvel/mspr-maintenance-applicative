const express = require('express');

const router = express.Router();
const moment = require('moment');
const basedService = require('../services/basedService');

/**
 * @api {get} /based/:date Retourne les horaires pour le jour souhaité
 * @apiName Based
 * @apiGroup Based
 *
 * @apiParam {Date} date Date (DD-MM-YY) requested
 *
 * @apiSuccess {String} district Numero d'arrondissement
 * @apiSuccess {Time} end  heure d'extinction
 * @apiSuccess {Time} start  heure d'allumage
 *
 * @apiError DateError not found No data for this date
 */
router.get('/:date', (req, res) => {
    const date = moment(req.params.date, 'DD-MM-YYYY').format('YYYY-MM-DD');
    basedService.getHours(date).then((results) => {
        res.send(results);
    }).catch(() => {
        res.status(404).send({ message: 'No data for this date' });
    });
});

module.exports = router;
