const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cron = require('node-cron');
const shell = require('shelljs');

const basedRouter = require('./routes/based');
const sedatifRouter = require('./routes/sedatif');

const app = express();

// exécute la tâche tous les jours à 1h du matin
cron.schedule('0 1 * * *', () => {
    console.log('run based script');
    if (shell.exec('node ./cronScript/based.js').code !== 0) {
        console.log('an error has occured');
        shell.exit(1);
    }
    else {
        console.log('based script completed');
    }

    console.log('run sedatif script');
    if (shell.exec('node ./cronScript/sedatif.js').code !== 0) {
        console.log('an error has occured');
        shell.exit(1);
    }
    else {
        console.log('sedatif script completed');
    }
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/based', basedRouter);
app.use('/sedatif', sedatifRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
    next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // send error
    res.status(err.status || 500);
    res.send(err);
});

module.exports = app;
