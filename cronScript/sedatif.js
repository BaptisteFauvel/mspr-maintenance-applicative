const fs = require('fs');
const parser = require('xml2json');
const { Pool } = require('pg');
const moment = require('moment');
require('dotenv').config();

const pool = new Pool({
    user: 'admin',
    host: '127.0.0.1',
    database: 'maintenance-db',
    password: process.env.DB_PASS,
    port: 5432
});

console.log('execution of the sedatif script in progress ...');

// Méthode récupérant tous les paths des fichiers dans un répertoire donné
const getAllFilesFromFolder = function (dir) {
    const results = [];

    fs.readdirSync(dir).forEach((file) => {
        file = `${dir}/${file}`;
        results.push(file);
    });

    return results;
};

// Récupération de tous les répertoires
const directories = getAllFilesFromFolder(`${__dirname}/../public/xml/SEDATIF`);

const files = [];

// Pour chaque répertoire, récupération de chaque fichier
directories.forEach((dir) => {
    const dirFiles = getAllFilesFromFolder(dir),
        dirName = dir.substring(dir.lastIndexOf('/') + 1),
        // Formatage des données par répertoire
        file = {
            name: dirName,
            files: dirFiles
        };
    files.push(file);
});

const options = {
    object: false,
    reversible: false,
    coerce: false,
    sanitize: true,
    trim: true,
    arrayNotation: false,
    alternateTextNode: false
};

// Pour chaque répertoire
files.forEach((file) => {
    // Récupération du fichier d'init
    const initFile = file.files.filter((fileName) => fileName.includes('init'));
    // Lecture du fichier d'init
    fs.readFile(initFile[0], (err, data) => {
    // Parsing du fichier d'init en JSON
        const init = JSON.parse(parser.toJson(data, options));
        // Insertion, si non existant, de l'espace
        pool.query('INSERT INTO spaces (id, name, address) VALUES ($1, $2, $3) ON CONFLICT DO NOTHING RETURNING *', [
            init.Document.Chantier.Espace.Id,
            init.Document.Chantier.Espace.Nom,
            init.Document.Chantier.Espace.Adresse
        ]).then(() => {
            // Si l'estpace a une tache
            if (init.Document.Chantier.Tache !== undefined) {
                // Si tache unique, transformation en tableau
                if (!Array.isArray(init.Document.Chantier.Tache)) {
                    init.Document.Chantier.Tache = [init.Document.Chantier.Tache];
                }
                const taskInsert = new Promise((resolve, reject) => {
                    // Insertion de chaque tache (state "terminée" = true)
                    init.Document.Chantier.Tache.forEach((task) => {
                        const endDate = moment(task.DateFin, 'DD-MM-YYYY').toDate();
                        pool.query('INSERT INTO tasks (name, space, state, end_date) VALUES ($1, $2, $3, $4) ON CONFLICT DO NOTHING RETURNING *', [
                            task.Nom,
                            init.Document.Chantier.Espace.Id,
                            task.Etat === 'Terminée',
                            endDate
                        ]);
                    });
                    resolve();
                });

                // Une fois les tasks insérées
                taskInsert.then(() => {
                    // Vérification de la présence de fichier d'update
                    if (file.files.filter((fileName) => fileName.includes('modif')).length > 0) {
                        // Récupération des fichiers d'update
                        const updateFiles = file.files.filter((fileName) => fileName.includes('modif')).map((updateFile) => {
                            const updateDate = moment(updateFile
                                .substring(updateFile.lastIndexOf('/') + 1)
                                .slice(0, -4)
                                .substring(6), 'DD-MM-YYYY')
                                .toDate();
                            // Formatage des dates d'update pour faciliter l'iteration plus tard
                            return {
                                path: updateFile,
                                updateDate
                            };
                        });

                        // Tri des fichiers d'update (plus ancien en premier)
                        const sortedUpdates = updateFiles.sort((a, b) => a.updateDate - b.updateDate);
                        sortedUpdates.forEach((updateFile) => {
                            fs.readFile(updateFile.path, (error, fileData) => {
                                const update = JSON.parse(parser.toJson(fileData, options));
                                // Récupération des informations de l'update
                                const updateData = update.Document.Chantier;
                                // Si l'espace est modifié
                                if (updateData.Espace !== undefined) {
                                    // Récupération des infos initiales
                                    pool.query('SELECT * FROM spaces WHERE id = $1', [updateData.Espace.Id]).then((res) => {
                                        // Update des informations de l'espace
                                        pool.query('UPDATE spaces SET name = $1, address = $2 WHERE id = $3', [
                                            // Si pas de nouvelle information, restitution de l'ancienne valeur
                                            // eslint-disable-next-line max-len
                                            updateData.Espace.Nom !== undefined ? updateData.Espace.Nom : res.rows[0].name,
                                            // eslint-disable-next-line max-len
                                            updateData.Espace.Adresse !== undefined ? updateData.Espace.Adresse : res.rows[0].address,
                                            updateData.Espace.Id
                                        ]).then(() => {
                                            if (updateData.Tache !== undefined) {
                                                if (!Array.isArray(updateData.Tache)) {
                                                    updateData.Tache = [updateData.Tache];
                                                }
                                                updateData.Tache.forEach((task) => {
                                                    const endDate = moment(task.DateFin, 'DD-MM-YYYY').toDate();
                                                    /* Insertion de la tache -> Si existante
                          , update statut de la tache avec la nouvelle valeur */
                                                    pool.query('INSERT INTO tasks (name, space, state, end_date) VALUES ($1, $2, $3, $4) ON CONFLICT ON CONSTRAINT tasks_pk DO UPDATE SET state = $3 RETURNING *', [
                                                        task.Nom,
                                                        init.Document.Chantier.Espace.Id,
                                                        task.Etat === 'Terminée',
                                                        endDate
                                                    ]);
                                                });
                                            }
                                        });
                                    });
                                }
                            });
                        });
                    }
                });
            }
        });
    });
});
