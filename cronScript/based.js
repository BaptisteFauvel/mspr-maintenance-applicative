const fs = require('fs');
const parser = require('xml2json');
const { Pool } = require('pg');
const moment = require('moment');
require('dotenv').config();

console.log('execution of the based script in progress ...');

const pool = new Pool({
    user: 'admin',
    host: '127.0.0.1',
    database: 'maintenance-db',
    password: process.env.DB_PASS,
    port: 5432
});

// Méthode récupérant tous les paths des fichiers dans un répertoire donné
const getAllFilesFromFolder = function (dir) {
    let results = [];

    fs.readdirSync(dir).forEach((file) => {
        file = `${dir}/${file}`;
        const stat = fs.statSync(file);

        if (stat && stat.isDirectory()) {
            results = results.concat(getAllFilesFromFolder(file));
        }
        else results.push(file);
    });

    return results;
};

// Récupération des fichiers dans le répertoire
const files = getAllFilesFromFolder(`${__dirname}/../public/xml/BASED`);

const options = {
    object: false,
    reversible: false,
    coerce: false,
    sanitize: true,
    trim: true,
    arrayNotation: false,
    alternateTextNode: false
};

// Récupération du path du fichier de localisation des éclairages
const lpdreFile = files.filter((file) => file.includes('lclEclr'));
const districtInsert = new Promise((resolve, reject) => {
    // Lecture du fichier de localisation
    fs.readFile(lpdreFile[0], (err, data) => {
    // Parsing des données en JSON
        const lpdreData = JSON.parse(parser.toJson(data, options));
        // Pour chaque Arrondissement -> création de l'arrondissement en base
        lpdreData.Document.Ctry.Rg.Dpt.Arrdt.forEach((arrdt) => {
            pool.query('INSERT INTO districts (id) VALUES ($1) ON CONFLICT DO NOTHING RETURNING *', [
                arrdt.Code
            ]).then(() => {
                // Vérification que l'arrondissement a bien des élairages
                if (arrdt.Lpdre !== undefined) {
                    // Si éclairage seul, transformation de la donnée en tableau
                    if (!Array.isArray(arrdt.Lpdre)) {
                        arrdt.Lpdre = [arrdt.Lpdre];
                    }
                    // Pour chaque lampadaire, insertion de la donnée en base
                    arrdt.Lpdre.forEach((lpdre) => {
                        pool.query('INSERT INTO lamps (id, district, latitude, longitude) VALUES ($1, $2, $3, $4) ON CONFLICT DO NOTHING RETURNING *', [
                            lpdre.Id, arrdt.Code, lpdre.Ltd, lpdre.Lgtd
                        ]);
                    });
                }
            });
        });
    });
    resolve();
});

// Récupération de la date du jour
let today = new Date();
let dd = today.getDate();
let mm = today.getMonth() + 1;
const yyyy = today.getFullYear();

if (dd < 10) {
    dd = `0${dd}`;
}
if (mm < 10) {
    mm = `0${mm}`;
}

// Formatage de la date du jour pour la récupération du fichier d'horaires du jour
today = `${dd}-${mm}-${yyyy}`;
// TODO change with today
// Récupération du fichier du jour
const dateFile = files.filter((file) => file.includes('07-02-2021'));

// Attente de l'insertion des arrondissements et éclairages
districtInsert.then(() => {
    // Lecture du fichier du jour
    fs.readFile(dateFile[0], (err, data) => {
    // Parsing en JSON
        const dateData = JSON.parse(parser.toJson(data, options));
        // Pour chaque arrondissement, insertion des horaires du jour
        dateData.Document.Arrdt.forEach((arrdt) => {
            // TODO multiple hours
            pool.query('INSERT INTO hours (day, district, "end", start) VALUES ($1, $2, $3, $4) ON CONFLICT DO NOTHING RETURNING *', [
                moment(today, 'DD-MM-YYYY').toDate(), arrdt.Id, arrdt.Plge[0].Fn, arrdt.Plge[arrdt.Plge.length - 1].Db
            ]);
        });
    });
});
