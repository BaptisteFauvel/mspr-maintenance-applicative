const { Pool } = require('pg');
require('dotenv').config();

const pool = new Pool({
    user: 'admin',
    host: '127.0.0.1',
    database: 'maintenance-db',
    password: process.env.DB_PASS,
    port: 5432
});

module.exports = pool;
